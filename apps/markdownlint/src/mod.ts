import { expandGlob } from 'std/fs/mod.ts';
import { relative } from 'std/path/mod.ts';
import * as YAML from 'std/yaml/mod.ts';

import markdownlintLib from 'markdownlint';
const { promises: { markdownlint } } = markdownlintLib;

// TODO: add error handling
export async function run() {
    const configFile = Deno.env.get('PARAM_CONFIGFILE');
    const config = {
        ...(configFile ? YAML.parse(await Deno.readTextFile(configFile)) as any : {}),
        ...(YAML.parse(Deno.env.get('PARAM_CONFIG') ?? '{}') as any),
    };
    const globs = YAML.parse(Deno.env.get('PARAM_FILES')!) as string[];
    const results = Object.entries(
        await markdownlint({
            config,
            files: (await Promise.all(
                globs
                    .map((glob) => expandGlob(glob, { globstar: true, includeDirs: false }))
                    .map((it) => Array.fromAsync(it)),
            )).flat().map(({ path }) => path),
        }),
    ).map(([file, errors]) => ({
        // use relative path for security reasons
        file: relative('.', file),
        errors,
    }));
    Deno.writeTextFile(Deno.env.get('RESULT_RESULT')!, `${results.every(({ errors }) => !errors.length)}`);
    Deno.writeTextFile(Deno.env.get('RESULT_DETAILS')!, 'markdownlint details:');
    Deno.writeTextFile(
        Deno.env.get('RESULT_TEXT')!,
        [
            '```log',
            ...results.flatMap(({ file, errors }) =>
                errors.sort((a, b) => a.lineNumber - b.lineNumber).map((
                    { lineNumber, ruleNames: [id, name], ruleDescription, errorRange },
                ) => {
                    const [startCol, endCol] = errorRange ?? [0, 0];
                    const range = errorRange ? `${startCol}-${endCol}:` : '';
                    return `${file}:${lineNumber}:${range} ${ruleDescription} (${id}/${name})`;
                })
            ),
            '```',
        ].join('\n'),
    );
}
