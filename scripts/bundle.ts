import { bundle } from 'libs/bundle/mod.ts';

const ROOT = new URL('../', import.meta.url);

for (const app of Deno.args) {
    await bundle(new URL(`apps/${app}/main.ts`, ROOT), new URL(`dist/${app}.js`, ROOT), {
        sourceMap: true,
        importMap: new URL('deno.jsonc', ROOT),
        minify: true,
    });
}
